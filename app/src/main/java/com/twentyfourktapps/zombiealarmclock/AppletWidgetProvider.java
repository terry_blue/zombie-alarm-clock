package com.twentyfourktapps.zombiealarmclock;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.Log;
import android.widget.RemoteViews;
import com.twentyfourktapps.zombiealarmclock.R;

public class AppletWidgetProvider extends AppWidgetProvider {

	private static SimpleDateFormat df = new SimpleDateFormat("h:mm a");
	final static String CLOCK_WIDGET_UPDATE = "com.tfkt.widget.clock_WIDGET_UPDATE";
	final static String TAG = "AppletWidgetProvider";
	
	public static void updateAppWidget(Context pContext, AppWidgetManager appWidgetManager, int appWidgetId) {
		DatabaseInterface DI = new DatabaseInterface(pContext);
		final boolean isAlarmEnabled = DI.isAlarmEnabled();
		DI.closeDB();
		RemoteViews views = new RemoteViews(pContext.getPackageName(), R.layout.widget_layout);

		Bitmap bmp = (isAlarmEnabled) ? BitmapFactory.decodeResource(pContext.getResources(), R.drawable.icon3) : BitmapFactory.decodeResource(pContext.getResources(), R.drawable.noalpha);
		
		views.setImageViewBitmap(R.id.widget_indicatorview, bmp);
		views.setImageViewBitmap(R.id.widget_timeview, updatedTimeBitmap(pContext, df.format(new Date())));
		appWidgetManager.updateAppWidget(appWidgetId, views);
		
	}

	private PendingIntent createClockTickIntent(Context pContext) {
		Intent intent = new Intent(CLOCK_WIDGET_UPDATE);
    	return PendingIntent.getBroadcast(pContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
	}
	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		// Perform this loop procedure for each App Widget that belongs to this
		// provider
        for (int appWidgetId : appWidgetIds) {
            // Get the layout for the App Widget and attach an on-click listener
            // to the button
            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_layout);

            //Create intent and widget onClickListener to point to settings activity
            Intent mIntent = new Intent(context, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, mIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            views.setOnClickPendingIntent(R.id.widget_layout_root, pendingIntent);


            // Tell the AppWidgetManager to perform an update on the current app
            // widget
            appWidgetManager.updateAppWidget(appWidgetId, views);


            // Update The clock label using a shared method
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
	}
	
	@Override
	public void onEnabled(Context pContext) {
		super.onEnabled(pContext);
		AlarmManager alarmManager = (AlarmManager)pContext.getSystemService(Context.ALARM_SERVICE);
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.add(Calendar.SECOND, 2);
		alarmManager.setRepeating(AlarmManager.RTC, calendar.getTimeInMillis(), 2000, createClockTickIntent(pContext));

	}
	
	@Override
	public void onDisabled(Context pContext) {
		super.onDisabled(pContext);
		AlarmManager alarmManager = (AlarmManager)pContext.getSystemService(Context.ALARM_SERVICE);
		alarmManager.cancel(createClockTickIntent(pContext));
		
	}
	
	@Override
	public void onReceive(Context pContext, Intent pIntent) {
		super.onReceive(pContext, pIntent);
		if(CLOCK_WIDGET_UPDATE.equals(pIntent.getAction())) {
			ComponentName thisAppWidget = new ComponentName(pContext.getPackageName(), getClass().getName());
			AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(pContext);
			int ids[] = appWidgetManager.getAppWidgetIds(thisAppWidget);
			for (int appWidgetID : ids)
				updateAppWidget(pContext, appWidgetManager, appWidgetID);
		}
	}
	
	public static Bitmap updatedTimeBitmap(Context pContext, String text) {
		Bitmap bmp = Bitmap.createBitmap(400, 120, Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(bmp);
		Paint paint = new Paint();
		Typeface typeface = Typeface.createFromAsset(pContext.getAssets(), "plasdrip.ttf");
		canvas.drawColor(0x90FFFFFF);
		paint.setAntiAlias(true);
		paint.setSubpixelText(true);
		paint.setTypeface(typeface);
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(Color.RED);
		paint.setTextSize(86);
		float textX = (canvas.getWidth() / 2) - (paint.measureText(text) / 2);
		canvas.drawText(text, textX, 80, paint);
		return bmp;
	}
}
