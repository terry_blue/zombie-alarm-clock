package com.twentyfourktapps.zombiealarmclock;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

public class AlarmActivity extends Activity {
	
	final String TAG = "AlarmActivity";
	SoundManager soundManager;
	boolean isThreadRunning, isMoveUp;
	Bitmap[] imageBMPs;
	int curImage, alarmRecId;
	ImageView bckImage;
	AlarmObject curAlarm;
	private PowerManager.WakeLock wakeLock;
	private Vibrator vibrator;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		PowerManager powerManager = (PowerManager)getSystemService(Context.POWER_SERVICE);
		wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "DoNotDimScreen");
		
		
		vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
		long[] vPattern = new long[] {0, 200, 100, 300, 100, 200, 300};
		vibrator.vibrate(vPattern, 0);
		setContentView(R.layout.alarm_layout);
		
		alarmRecId = getIntent().getExtras().getInt("com.twentyfourktapps.zombiealarm.intentalarmrecid");
		DatabaseInterface DI = new DatabaseInterface(AlarmActivity.this);
		//DI.logAllAlarms();
		curAlarm = DI.getAlarmForRecID(alarmRecId);
		DI.closeDB();
		
		//if(curAlarm != null) Log.e(TAG, "curAlarm != null, alarmRecId = " + alarmRecId);
		//else Log.e(TAG, "CurAlarm == null, alarmRecId = " + alarmRecId);
				
		loadImagesToMem();
		setButtonListeners();
		
		if(!wakeLock.isHeld()) wakeLock.acquire();
		List<String> rawSoundsArray = Arrays.asList(getResources().getStringArray(R.array.sounds_array));
		int soundId = rawSoundsArray.indexOf(curAlarm.sound) - 1;
		
		if(soundId < 0) {
			Random rand = new Random();
			int index = rand.nextInt() % SoundManager.ALL_SOUNDS.length;
			if (index < 0) index *= -1;			
			soundManager = new SoundManager(AlarmActivity.this, index);
		} else {
			soundManager = new SoundManager(AlarmActivity.this, soundId);
		}
		soundManager.play();
		
		
		setImageFlicker();
		
	}
	

	@Override
	public void onResume() {
		super.onResume();
		setButtonListeners();
		
		DatabaseInterface DI = new DatabaseInterface(AlarmActivity.this);
		//Consume spent alarms
		DI.consumeSpentAlarms();
		//get and set next alarm
		
		DI.getNextAlarm(AlarmActivity.this);
		DI.closeDB();
	}
	
	@Override
	public void onStop() {
		super.onStop();
		if(imageBMPs[0] != null) {
			imageBMPs[0].recycle();
			imageBMPs[0] = null;
		}
		if(imageBMPs[1] != null) {
			imageBMPs[1].recycle();
			imageBMPs[1] = null;
		}
		if(imageBMPs[2] != null) {
			imageBMPs[2].recycle();
			imageBMPs[2] = null;
		}
		if(vibrator != null) vibrator.cancel();
		
		
		if(soundManager != null) soundManager.stopAllSounds();
		
		if(wakeLock != null) wakeLock.release();
		
		if(soundManager != null) soundManager.stopAllSounds();
		
		
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		AlarmActivity.this.stopService(new Intent(AlarmActivity.this, AlarmService.class));
		
	}
	private void setButtonListeners() {
		Typeface font = Typeface.createFromAsset(getAssets(), "feasfbrg.ttf");
		
		Button dismissButton = (Button)findViewById(R.id.alarm_activity_button_dismiss);
		dismissButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				
				isThreadRunning = false;
				finish();
				
			}
		});
		dismissButton.setTypeface(font);
		
		Button snoozeButton = (Button)findViewById(R.id.alarm_activity_button_snooze);
		snoozeButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				
				Calendar c = Calendar.getInstance();
				c.set(Calendar.SECOND, 0);
				
				try {
					DatabaseInterface DI = new DatabaseInterface(AlarmActivity.this);
					DI.insertAlarm(-1, "Snooze", c.getTimeInMillis() + (curAlarm.snoozeTime * 60000), false, curAlarm.isVibrate, curAlarm.snoozeTime, (curAlarm.maxSnoozes - 1), curAlarm.sound);
					//DI.logAllAlarms();
					DI.closeDB();
				} catch(Exception e) {
					Log.e(TAG, "snooze onCLick error: " + e.toString());
				}
				
				isThreadRunning = false;
				finish();
			}
		});
		snoozeButton.setTypeface(font);
		
		if(curAlarm.maxSnoozes < 1) {
			snoozeButton.setEnabled(false);
		}
	}
	private void setImageFlicker() {
		bckImage = (ImageView)findViewById(R.id.alarm_activity_imageview_creepy);
		isThreadRunning = true;
		isMoveUp = true;
		ImageSwitchThread newThread = new ImageSwitchThread();
		newThread.start();
	}
	
	
	private void loadImagesToMem() {
		imageBMPs = new Bitmap[3];	
		
		int[][] ids = new int[5][3];
		
		ids[0][0] = R.drawable.zombieartwork_1_image_1;
		ids[0][1] = R.drawable.zombieartwork_1_image_2;
		ids[0][2] = R.drawable.zombieartwork_1_image_3;
		
		ids[1][0] = R.drawable.zombieartwork_2_image_1;
		ids[1][1] = R.drawable.zombieartwork_2_image_2;
		ids[1][2] = R.drawable.zombieartwork_2_image_3;
		
		ids[2][0] = R.drawable.zombieartwork_3_image_1;
		ids[2][1] = R.drawable.zombieartwork_3_image_2;
		ids[2][2] = R.drawable.zombieartwork_3_image_3;
		
		ids[3][0] = R.drawable.zombieartwork_4_image_1;
		ids[3][1] = R.drawable.zombieartwork_4_image_2;
		ids[3][2] = R.drawable.zombieartwork_4_image_3;
		
		ids[4][0] = R.drawable.zombieartwork_5_image_1;
		ids[4][1] = R.drawable.zombieartwork_5_image_2;
		ids[4][2] = R.drawable.zombieartwork_5_image_3;
		
		Random rand = new Random();
		int column = rand.nextInt() % 5;
		if(column < 0) column *= -1;
		
		imageBMPs[0] = BitmapFactory.decodeResource(getResources(), ids[column][0]);
		imageBMPs[1] = BitmapFactory.decodeResource(getResources(), ids[column][1]);
		imageBMPs[2] = BitmapFactory.decodeResource(getResources(), ids[column][2]);
	}
	
	
	
	
	Handler ImageSwitchHandler = new Handler() {
		
		@Override
		public void handleMessage(Message msg) {
			
			if(isMoveUp) {
				curImage++;
				if(curImage == 2) isMoveUp = false;
			}
			else {
				curImage--;
				if(curImage == 0) isMoveUp = true;
			}
			bckImage.setImageBitmap(imageBMPs[curImage]);
			 
		}
	};
	private class ImageSwitchThread extends Thread {
		
		@Override 
		public void run() {
			while(isThreadRunning) {
				try { sleep(200); } catch(Exception e) { Log.e("ALARM ACTIVITY THREAD", e.toString()); }
				
				Message msg = new Message();
				msg.obj = "1";
				ImageSwitchHandler.sendMessage(msg);
			}
		}
	}
}
