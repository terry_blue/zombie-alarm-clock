package com.twentyfourktapps.zombiealarmclock;

import java.util.HashMap;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.SoundPool;
import android.preference.PreferenceManager;
import android.util.Log;

import com.twentyfourktapps.zombiealarmclock.R;

public class SoundManager {
	final String TAG = "SoundManager";
	
	//Sound IDs
	final static int CREEPY = 1;
	final static int HELP = 2;
	final static int NOOO = 3;
	final static int RIOT = 4;
	final static int RIOT_2 = 5;
	final static int SCREAM = 6;
	final static int SCREAM_2 = 7;
	final static int SCREAM_3 = 8;
	final static int SCREAM_4 = 9;
	final static int SCREAM_5 = 10;
	final static int SCREAM_6 = 11;
	final static int SCREAM_7 = 12;
	final static int SCREAM_8 = 13;
	final static int ZOMBIE = 14;
	final static int ZOMBIE_2 = 15;
	final static int ZOMBIE_3 = 16;
	final static int ZOMBIE_4 = 17;
	final static int ZOMBIE_5 = 18;
	final static int ZOMBIE_GROUP = 19;
	final static int ZOMBIE_GROUP_2 = 20;
	
	final static int[] ALL_SOUNDS = new int[] { CREEPY, HELP, NOOO, SCREAM, SCREAM_2, SCREAM_3, SCREAM_4, SCREAM_5, SCREAM_6, SCREAM_7, SCREAM_8, ZOMBIE, ZOMBIE_2, ZOMBIE_3, ZOMBIE_4, ZOMBIE_5 };
	final int[] SOUND_IDS = new int[] { R.raw.creepy, R.raw.help, R.raw.noooo, R.raw.scream, R.raw.scream, R.raw.scream2, R.raw.scream3, R.raw.scream4, R.raw.scream5, R.raw.scream6, R.raw.scream7, R.raw.scream8, R.raw.zombie, R.raw.zombie2, R.raw.zombie3, R.raw.zombie4, R.raw.zombie5 };
	
	Context context;
	public SoundPool soundPool;
	public HashMap<Integer, Integer> soundPoolMap;
	boolean isSoundEnabled;
	int curSoundID = 0;
	
	public SoundManager(Context pContext) {
		context = pContext;
		soundPool = new SoundPool(1, AudioManager.STREAM_ALARM, 0);
		soundPoolMap = new HashMap<Integer, Integer>();
	
		//check to see if the sound is enabled
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		isSoundEnabled = sp.getBoolean("soundEnabled", true);
		
		initializeAllSounds();
	}
	
	public SoundManager(Context pContext, int finalSound) {
		context = pContext;
		soundPool = new SoundPool(1, AudioManager.STREAM_ALARM, 0);
		soundPoolMap = new HashMap<Integer, Integer>();
	
		//check to see if the sound is enabled
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		isSoundEnabled = sp.getBoolean("soundEnabled", true);
		
		curSoundID = finalSound;
		initializeSound(finalSound, SOUND_IDS[finalSound], 1);
	}
	
	
	public void initializeSound(int soundID,  int rawId, int priority) {
		soundPoolMap.put(soundID, soundPool.load(context, rawId, priority));
	}
	public void playSound(int sound) {
		
		if (isSoundEnabled) {
			/* Updated: The next 4 lines calculate the current volume in a scale of 0.0 to 1.0 */
		    AudioManager mgr = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
		    float streamVolumeCurrent = mgr.getStreamVolume(AudioManager.STREAM_ALARM);
		    float streamVolumeMax = mgr.getStreamMaxVolume(AudioManager.STREAM_ALARM);    
		    float volume = streamVolumeCurrent / streamVolumeMax;
		    
		    /* Play the sound with the correct volume */
            int retryCount = 0;
		    while (soundPool.play(soundPoolMap.get(sound), volume, volume, 1, -1, 1f) == 0) {
		    	Log.e(TAG, "Failed to play sound number " + sound + ".... retrying...");
                if(retryCount++ > 1000)
                    break;
		    }
		}
	}

	public void initializeAllSounds() {
		initializeSound(CREEPY, R.raw.creepy, 1);
		initializeSound(HELP, R.raw.help, 1);
		initializeSound(NOOO, R.raw.noooo, 1);
		initializeSound(SCREAM, R.raw.scream, 1);
		initializeSound(SCREAM_2, R.raw.scream2, 1);
		initializeSound(SCREAM_3, R.raw.scream3, 1);
		initializeSound(SCREAM_4, R.raw.scream4, 1);
		initializeSound(SCREAM_5, R.raw.scream5, 1);
		initializeSound(SCREAM_6, R.raw.scream6, 1);
		initializeSound(SCREAM_7, R.raw.scream7, 1);
		initializeSound(SCREAM_8, R.raw.scream8, 1);
		initializeSound(ZOMBIE, R.raw.zombie, 1);
		initializeSound(ZOMBIE_2, R.raw.zombie2, 1);
		initializeSound(ZOMBIE_3, R.raw.zombie3, 1);
		initializeSound(ZOMBIE_4, R.raw.zombie4, 1);
		initializeSound(ZOMBIE_5, R.raw.zombie5, 1);
		
	}
	
	public void stopAllSounds() {
		isSoundEnabled = false;
		try {
            for (int ALL_SOUND : ALL_SOUNDS)
                soundPool.stop(ALL_SOUND);
			soundPool.release();
			soundPool = null;
		} catch(Exception e) { Log.e(TAG, "stop all sounds error: " + e.toString()); 	}
	}

	
	public void play() {
		if(curSoundID != -1) {
			playSound(curSoundID);
		}
	}
}
