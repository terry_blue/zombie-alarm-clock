package com.twentyfourktapps.zombiealarmclock;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class AlarmService extends Service{

	final String TAG = "AlarmService";
	boolean intentSent;
	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		intentSent = false;
		
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		//Toast.makeText(this, TAG + " onStartCommand", Toast.LENGTH_SHORT).show();
		Intent mIntent = new Intent(getBaseContext(), AlarmActivity.class);
		//Log.e("AlarmService", "onStartCommand, startId = " + startId);
		
		int recID = intent.getExtras().getInt("com.twentyfourktapps.zombiealarm.intentalarmrecid");
		mIntent.putExtra("com.twentyfourktapps.zombiealarm.intentalarmrecid", recID);
		mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		getApplication().startActivity(mIntent);
		return START_REDELIVER_INTENT;
	}
	
	@Override
	public void onDestroy() { 
		super.onDestroy();
		//Toast.makeText(this, TAG + " onDestroy", Toast.LENGTH_SHORT).show();
	}
	
	@Override
	public void onStart(Intent intent, int intentId) {
		super.onStart(intent, intentId);
		//Toast.makeText(this, TAG + " onStart", Toast.LENGTH_SHORT).show();
		
		
	}
	
	@Override
	public boolean onUnbind(Intent intent) {
		//Toast.makeText(this, TAG + " onUnbind", Toast.LENGTH_SHORT).show();
		return super.onUnbind(intent);
	}
	
	
}
