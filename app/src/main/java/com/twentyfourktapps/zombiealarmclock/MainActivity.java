package com.twentyfourktapps.zombiealarmclock;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
//import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends Activity {

	Context context;
	final String TAG = "MainActivity";
	AlarmBundle[] alarms;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        setButtonListeners();
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	createListView(); 
    	setNextAlarmView();
    	   	
    }
    @Override
    protected void onStop() {
    	super.onStop();
    	//setNextAlarmView();
    }

   private void setButtonListeners() {
	  
	   Button startButton = (Button)findViewById(R.id.button_set);
       startButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				DatabaseInterface DI = new DatabaseInterface(context);
				int newAlarmNum = DI.getNewAlarmNumber();
				//Log.e("startButton onClick", "new alarm number = " + newAlarmNum);
				//DI.logAllAlarms();
				DI.closeDB();
				
				Intent mIntent = new Intent(context, AddAlarmActivity.class);
				mIntent.putExtra("com.twentyfourktapps.zombiealarmclock.AddAlarmActivity.isNew", true);
				mIntent.putExtra("com.twentyfourktapps.zombiealarmclock.AddAlarmActivity.alarmNum", newAlarmNum);
				startActivity(mIntent);
			}
		});
       Typeface font = Typeface.createFromAsset(getAssets(), "feasfbrg.ttf");
       startButton.setTypeface(font);
       
       Button cancelButton = (Button)findViewById(R.id.button_cancel);
       cancelButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				DatabaseInterface DI = new DatabaseInterface(MainActivity.this);
				int alarmID = DI.getNextAlarm(MainActivity.this).recID;
				//Log.e(TAG, "cancelButtonPress alarm id = " + alarmID);
				DI.logAllAlarms();
				DI.closeDB();
  
				Intent mIntent = new Intent(context, AlarmActivity.class);
				mIntent.putExtra("com.twentyfourktapps.zombiealarm.intentalarmrecid", alarmID);
				startActivity(mIntent);
			}
		});
   }
   public void setNextAlarmView() {
	   DatabaseInterface DI = new DatabaseInterface(context);
	   AlarmObject nextAlarm = DI.getNextAlarm(MainActivity.this);
	   DI.closeDB();
	   
	   Typeface font = Typeface.createFromAsset(getAssets(), "feasfbrg.ttf");
	   String nextAlarmString = "No Alarms Set\n";
	   if(nextAlarm != null) {
		   Calendar c = Calendar.getInstance();
		   c.setTimeInMillis(nextAlarm.timeMillis);
		   StringBuilder sb = new StringBuilder();
		   sb.append("Next Alarm\n");
		   switch(c.get(Calendar.DAY_OF_WEEK)) {
		   case Calendar.SUNDAY:
			   sb.append("Sunday");
			   break;
		   case Calendar.MONDAY:
			   sb.append("Monday");
			   break;
		   case Calendar.TUESDAY:
			   sb.append("Tuesday");
			   break;
		   case Calendar.WEDNESDAY:
			   sb.append("Wednesday");
			   break;
		   case Calendar.THURSDAY:
			   sb.append("Thursday");
			   break;
		   case Calendar.FRIDAY:
			   sb.append("Friday");
			   break;
		   case Calendar.SATURDAY:
			   sb.append("Saturday");
			   break;
		   }
		   
		   sb.append(" at ");
		   SimpleDateFormat df = new SimpleDateFormat("h:mm a");
		   sb.append(df.format(new Date(nextAlarm.timeMillis)));
		   nextAlarmString = sb.toString();
	   } 
	   else {
		   //Log.e(TAG, "setNextAlarmView(), stopping service");
		   stopService(new Intent(MainActivity.this, AlarmService.class));
	   }
	   TextView nextAlarmTV = (TextView)findViewById(R.id.main_activity_textview_next_alarm);
	   nextAlarmTV.setText(nextAlarmString);
	   nextAlarmTV.setTypeface(font);
   }
   public void createListView() {
	   DatabaseInterface DI = new DatabaseInterface(context);
	   DI.getNextAlarm(MainActivity.this);
	   ArrayList<AlarmBundle> arrayListBundle = DI.getAlarmBundles();
	   ArrayList<AlarmBundle> displayBundles = DI.getAlarmBundles();
	   
	   for(int i = 0; i < displayBundles.size(); i++) {
		  // Calendar c = Calendar.getInstance();
		   if(displayBundles.get(i).alarmID == -1) {
			   displayBundles.remove(i);
		   }
	   }
	   AlarmBundle[] alarmBundles = displayBundles.toArray(new AlarmBundle[displayBundles.size()]);
	   alarms = arrayListBundle.toArray(new AlarmBundle[arrayListBundle.size()]);
	 
	   DI.closeDB();
	   
	   ListView listView = (ListView)findViewById(R.id.main_activity_listview);
	   BundleArrayAdaptor bundleAdaptor = new BundleArrayAdaptor(context, R.layout.alarm_budle_layout, alarmBundles, MainActivity.this);
	   listView.setAdapter(bundleAdaptor);
	   
	   registerForContextMenu(listView);
	   
	   listView.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
				try {
					Intent mIntent = new Intent(context, AddAlarmActivity.class);
					mIntent.putExtra("com.twentyfourktapps.zombiealarmclock.AddAlarmActivity.isNew", false);
					mIntent.putExtra("com.twentyfourktapps.zombiealarmclock.AddAlarmActivity.alarmNum", alarms[pos].alarmID);
					startActivity(mIntent);
				} catch (Exception e) {
					createListView();
					setNextAlarmView();
				}
			}
	   });
	   
	   LinearLayout rootLayout = (LinearLayout)findViewById(R.id.main_activity_layout_content_root);
	   rootLayout.setOnClickListener(new View.OnClickListener() {
		
		public void onClick(View v) {
			setNextAlarmView();
			
		}
	});
   }
   
  
   @Override
   public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
	   super.onCreateContextMenu(menu, v, menuInfo);
	   MenuInflater inflater = getMenuInflater();
	   inflater.inflate(R.menu.main_activity_delete, menu);
   }
   @Override
   public boolean onContextItemSelected(MenuItem item) {
	   AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
	   switch(item.getItemId()) {
	   case R.id.delete:
		   DatabaseInterface DI = new DatabaseInterface(context);
		   DI.deleteBundle(alarms[(int)info.id].alarmID);
		   DI.closeDB();
		 
		   createListView();
	   }
	   return true;
   }
  
  
}
