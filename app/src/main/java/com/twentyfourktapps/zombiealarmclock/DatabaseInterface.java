package com.twentyfourktapps.zombiealarmclock;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseInterface {

	final String TAG = "DatabaseInterface";
	DatabaseClass theDB;
	private final int DB_VERSION = 1;
	private final String DB_NAME = "com.twentyfourktapps.zombiealarmclock_DB";
	
	private final String TABLE_NAME = "Alarm_Table";
	private final String ROW_ID = "row_id";
	private final String ROW_LABEL = "alarm_label";
	private final String ROW_TIME = "time";
	private final String ROW_REPEAT = "repeat";
	private final String ROW_ALARM_ID = "alarm_id";
	private final String ROW_ENABLED = "enabled";
	private final String ROW_SOUND = "sound";
	private final String ROW_VIBRATE = "vibrate";
	private final String ROW_MAX_SNOOZES = "max_snoozes";
	private final String ROW_SNOOZE_TIME = "snooze_time";
	
	private final String[] TABLE_ROWS = new String[] { ROW_ID, ROW_LABEL, ROW_TIME, ROW_REPEAT, ROW_ALARM_ID, ROW_ENABLED, ROW_SOUND, ROW_VIBRATE, ROW_MAX_SNOOZES, ROW_SNOOZE_TIME };
	
	
	public DatabaseInterface(Context pContext) {
		theDB = new DatabaseClass(pContext, DB_NAME, null, DB_VERSION);
	}
	public void closeDB() {
		theDB.close();
	}
	
	public boolean isAlarmEnabled() {
		ArrayList<AlarmBundle> alarms = theDB.getAllAlarmBundles();
		for(int i = 0; i < alarms.size(); i++) {
			if(alarms.get(i).isEnabled) return true;
		}
		return false;
	}
	public int getNewAlarmNumber() {
		ArrayList<AlarmObject> alarms = theDB.getAllAlarms();
		int alarmNum = 0;
		for(int i = 0; i < alarms.size(); i++) {
			if(alarms.get(i).alarmID >= alarmNum) alarmNum = alarms.get(i).alarmID + 1;
		}
		return alarmNum;
	}
	public void insertAlarm(int alarmID, String label, long timeMillis, boolean repeat, boolean vibrate, int snoozeTime, int maxSnoozes, String sound) {
		theDB.insertAlarmTime(alarmID, label, timeMillis, repeat, vibrate, snoozeTime, maxSnoozes, sound);
	}
	public AlarmBundle getAlarmBundleForId(int alarmID) {		
		return new AlarmBundle(theDB.getAlarmsForID(alarmID));
	}
	public AlarmObject getNextAlarm(Activity activity) {
		
		AlarmObject nextAlarm = theDB.getNextAlarm();
		AlarmManager alarmManager = (AlarmManager)activity.getSystemService(Activity.ALARM_SERVICE);
		Intent mIntent = new Intent(activity, AlarmService.class);
		PendingIntent cancelPendingIntent = PendingIntent.getService(activity, 0, mIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		alarmManager.cancel(cancelPendingIntent);
		
		activity.stopService(mIntent);
		
		if(nextAlarm != null) {
		
			int recID = nextAlarm.recID;
			//Log.e(TAG, "putting recID extra: value = " + recID);
			mIntent.putExtra("com.twentyfourktapps.zombiealarm.intentalarmrecid", recID);
			cancelPendingIntent = PendingIntent.getService(activity, 0, mIntent, PendingIntent.FLAG_UPDATE_CURRENT);		
			Calendar c = Calendar.getInstance();
			if(c.getTimeInMillis() < nextAlarm.timeMillis)	alarmManager.set(AlarmManager.RTC_WAKEUP, nextAlarm.timeMillis, cancelPendingIntent);
			//Toast.makeText(activity, "Start Alarm", Toast.LENGTH_SHORT).show();
		}
		
		return nextAlarm;
	}
	public AlarmObject getAlarmForRecID(int alarmRecId) {	
		//Log.e(TAG, "getting alarm for " + alarmRecId);
		return theDB.getAlarmForRecID(alarmRecId);
	}
	public ArrayList<AlarmBundle> getAlarmBundles() {
		return theDB.getAllAlarmBundles();
	}
	public void setBundleEnabled(int alarmID, boolean enabled) {
		theDB.setBundleEnabled(alarmID, enabled);
	}
	public void deleteBundle(int alarmID) {
		theDB.deleteBundle(alarmID);
	}
	public void deleteAlarm(int recID) {
		theDB.deleteAlarm(recID);
	}
	public void consumeSpentAlarms() {
		ArrayList<AlarmObject> allAlarms = theDB.getAllAlarms();
		for(int i = 0; i < allAlarms.size(); i++) {
			Calendar c = Calendar.getInstance();
			if((allAlarms.get(i).timeMillis) < c.getTimeInMillis()) {
				if(allAlarms.get(i).repeat) {
					repeatAlarm(allAlarms.get(i));
				} else {
					deleteAlarm(allAlarms.get(i).recID);
				}				
			}
		}
	}
	public void repeatAlarm(AlarmObject alarm) {
		theDB.repeatAlarm(alarm);
	}
	//DEBUGGING METHOD
	public void logAllAlarms() {
		ArrayList<AlarmObject> alarms = theDB.getAllAlarms();
		for(int i = 0; i < alarms.size(); i++) {
			Calendar c = Calendar.getInstance();
			c.setTimeInMillis(alarms.get(i).timeMillis);
			Log.e("AlarmInfo", "alarm time/date = " + c.get(Calendar.HOUR) + ":" + c.get(Calendar.MINUTE) + " - " + c.get(Calendar.DATE) + "/" + Integer.toString(c.get(Calendar.MONTH) + 1) + ", alarmID = " + alarms.get(i).alarmID + ", recID = " + alarms.get(i).recID + ", isEnabled = " + Boolean.toString(alarms.get(i).isEnabled) + ", Number of snoozes = " + alarms.get(i).maxSnoozes);
		}
	}
	
	
	//DATABASE CLASS
	private class DatabaseClass extends SQLiteOpenHelper {

		private final String TAG = "DatabaseClass";
		protected SQLiteDatabase DB;
		
		public DatabaseClass(Context context, String name, CursorFactory factory, int version) {
			super(context, name, factory, version);
			DB = getWritableDatabase();
		}

		
		public void insertAlarmTime(int alarmID, String label, long timeInMillis, boolean repeat, boolean vibrate, int snoozeTime, int maxSnoozes, String sound) {
			String repeatString = (repeat) ? "true" : "false";
			String vibrateString = (vibrate) ? "true" : "false";
			ContentValues values = new ContentValues();
			values.put(ROW_ALARM_ID, alarmID);
			values.put(ROW_TIME, timeInMillis);
			values.put(ROW_REPEAT, repeatString);
			values.put(ROW_LABEL, label);
			values.put(ROW_ENABLED, "true");
			values.put(ROW_VIBRATE, vibrateString);
			values.put(ROW_MAX_SNOOZES, maxSnoozes);
			values.put(ROW_SNOOZE_TIME, snoozeTime);
			values.put(ROW_SOUND, sound);
			DB.insert(TABLE_NAME, null, values);
			Log.i(TAG, "Alarm added: " + Long.toString(timeInMillis) + " Repeat: " + Boolean.toString(repeat));
		}
		public AlarmObject getNextAlarm() {		
			//Log.e(TAG, "logging all alrams inside getNextAlarm()");
			//logAllAlarms();
			try {
				
				Cursor cursor = DB.query(TABLE_NAME, TABLE_ROWS, null, null, null, null, ROW_TIME + " ASC");
				cursor.moveToFirst();
				while(!cursor.isAfterLast()) {
					long curTime = System.currentTimeMillis();
					//Log.e(TAG, "getNextAlarm() inside while(!cursor.isAfterLast())");
					int recId = cursor.getInt(0);
					String alarmLabel = cursor.getString(1);
					long timeMillis = cursor.getLong(2);
					boolean isRepeat = Boolean.parseBoolean(cursor.getString(3));
					int alarmId = cursor.getInt(4);
					boolean isEnabled = Boolean.parseBoolean(cursor.getString(5));
					String soundString = cursor.getString(6);
					boolean isVibrate = Boolean.parseBoolean(cursor.getString(7));
					int maxSnoozes = cursor.getInt(8);
					int snoozeTime = cursor.getInt(9);
					
					if(curTime < (timeMillis)) {
						//return this alarm						
						if(isEnabled) {
							Log.e(TAG, "getNextAlarm() returning alarm recID: " + recId);
							return new AlarmObject(alarmId, recId, alarmLabel, true, isRepeat, timeMillis, soundString, isVibrate, maxSnoozes, snoozeTime);
						}
					} 
					cursor.moveToNext();
				}
				
				cursor.close();
			} catch(Exception e) {
				Log.e(TAG, "getNextAlarm error(): " + e.toString());
			}
			Log.e(TAG, "getNextAlarm() returns null");
			return null;
		}
		public AlarmObject getAlarmForRecID(int alarmRecID) {
			//Log.e(TAG, "getAlarmForRecID(), alarmRecID = " + alarmRecID);
			AlarmObject toReturn = null;
			String whereString = ROW_ID + " = " + Integer.toString(alarmRecID);
			try {
				Cursor cursor = DB.query(TABLE_NAME, TABLE_ROWS, whereString, null, null, null, null);
				cursor.moveToFirst();
				if(!cursor.isAfterLast()) {
					//Log.e(TAG, "getAlarmForRecId() inside if(!cursor.isAfterLast())");
					int recId = cursor.getInt(0);
					String alarmLabel = cursor.getString(1);
					long timeMillis = cursor.getLong(2);
					boolean isRepeat = Boolean.parseBoolean(cursor.getString(3));
					int alarmId = cursor.getInt(4);
					boolean isEnabled = Boolean.parseBoolean(cursor.getString(5));
					String soundString = cursor.getString(6);
					boolean isVibrate = Boolean.parseBoolean(cursor.getString(7));
					int maxSnoozes = cursor.getInt(8);
					int snoozeTime = cursor.getInt(9);
					
					toReturn = new AlarmObject(alarmId, recId, alarmLabel, isEnabled, isRepeat, timeMillis, soundString, isVibrate, maxSnoozes, snoozeTime);
				}
			} catch(Exception e) {
				Log.e(TAG, "getAlarmForRecID() error: " + e.toString());
			}
			return toReturn;
		}
		public ArrayList<Integer> getUniqueAlarmIDs() {
			ArrayList<Integer> ids = new ArrayList<Integer>();
			try {
				Cursor cursor = DB.query(TABLE_NAME, new String[] {ROW_ALARM_ID}, null, null, null, null, null);
				cursor.moveToFirst();
				while(!cursor.isAfterLast()) {
					boolean exists = false;
					int idNum = cursor.getInt(0);
					for(int i = 0; i < ids.size(); i++) {
						if(ids.get(i) == idNum) {
							exists = true;
						}
					}
					if(!exists) ids.add(idNum);
					cursor.moveToNext();
				}
			} catch(Exception e) {
				Log.e(TAG, "getUniqueIDs() error: " + e.toString());
			}
			
			return ids;
		}
		public ArrayList<AlarmObject> getAlarmsForID(int idNum) {
			ArrayList<AlarmObject> alarms = new ArrayList<AlarmObject>();
			try { 
				Cursor cursor = DB.query(TABLE_NAME, TABLE_ROWS, ROW_ALARM_ID + " = " + Integer.toString(idNum), null, null, null, null);
				cursor.moveToFirst();
				while(!cursor.isAfterLast()) {
					int recId = cursor.getInt(0);
					String alarmLabel = cursor.getString(1);
					long timeMillis = cursor.getLong(2);
					boolean isRepeat = Boolean.parseBoolean(cursor.getString(3));
					int alarmId = cursor.getInt(4);
					boolean isEnabled = Boolean.parseBoolean(cursor.getString(5));
					String soundString = cursor.getString(6);
					boolean isVibrate = Boolean.parseBoolean(cursor.getString(7));
					int maxSnoozes = cursor.getInt(8);
					int snoozeTime = cursor.getInt(9);
					
					alarms.add(new AlarmObject(alarmId, recId, alarmLabel, isEnabled, isRepeat, timeMillis, soundString, isVibrate, maxSnoozes, snoozeTime));
					cursor.moveToNext();
				}
			} catch(Exception e) {
				Log.e(TAG, "getAlarmsForID() error: " + e.toString());
			}
			return alarms;
		}
		public ArrayList<AlarmBundle> getAllAlarmBundles() {
			ArrayList<AlarmBundle> bundles = new ArrayList<AlarmBundle>();
			ArrayList<Integer> ids = getUniqueAlarmIDs();
			for(int i = 0; i < ids.size(); i++) {				
				bundles.add(new AlarmBundle(getAlarmsForID(ids.get(i))));				
			}
			
			return bundles;
		}
		public ArrayList<AlarmObject> getAllAlarms() {
			ArrayList<AlarmObject> alarms = new ArrayList<AlarmObject>();
			try {
				Cursor cursor = DB.query(TABLE_NAME, TABLE_ROWS, null, null, null, null, null);
				cursor.moveToFirst();
				while(!cursor.isAfterLast()) {
					int recId = cursor.getInt(0);
					String alarmLabel = cursor.getString(1);
					long timeMillis = cursor.getLong(2);
					boolean isRepeat = Boolean.parseBoolean(cursor.getString(3));
					int alarmId = cursor.getInt(4);
					boolean isEnabled = Boolean.parseBoolean(cursor.getString(5));
					String soundString = cursor.getString(6);
					boolean isVibrate = Boolean.parseBoolean(cursor.getString(7));
					int maxSnoozes = cursor.getInt(8);
					int snoozeTime = cursor.getInt(9);
					
					alarms.add(new AlarmObject(alarmId, recId, alarmLabel, isEnabled, isRepeat, timeMillis, soundString, isVibrate, maxSnoozes, snoozeTime));
					cursor.moveToNext();
				}
			} catch(Exception e) {
				Log.e(TAG, "getAllAlarms() error: " + e.toString());
			}
			return alarms;
		}
		public void setBundleEnabled(int alarmID, boolean enabled) {
			String enabledString = (enabled) ? "true" : "false";
			String alarmIdString = Integer.toString(alarmID);
			ContentValues values = new ContentValues();
			values.put(ROW_ENABLED, enabledString);
			
			DB.update(TABLE_NAME, values, ROW_ALARM_ID + " = " + alarmIdString, null);
		}
		public void deleteBundle(int alarmID) {
			String whereString = ROW_ALARM_ID + " = " + Integer.toString(alarmID);
			DB.delete(TABLE_NAME, whereString, null);
		}
		public void deleteAlarm(int recID) {
			if(recID != 0) {
				String whereString = ROW_ID + " = " + Integer.toString(recID);
				DB.delete(TABLE_NAME, whereString, null);
			}
		}
		public void repeatAlarm(AlarmObject alarm) {
			ContentValues values = new ContentValues();
			long newTime = alarm.timeMillis + 604800000;
			values.put(ROW_TIME, newTime);
			String whereString = ROW_ID + " = " + Integer.toString(alarm.recID);
			DB.update(TABLE_NAME, values, whereString, null);
		}
		
		@Override
		public void onCreate(SQLiteDatabase arg0) {
			DB = arg0;
			
			String newTableString = "CREATE TABLE " + TABLE_NAME +
					" (" + ROW_ID + " INTEGER PRIMARY KEY NOT NULL, " + 
					ROW_TIME + " BIGINT, " + 
					ROW_REPEAT + " TEXT, " +
					ROW_ENABLED + " TEXT, " +
					ROW_ALARM_ID + " INT, " +
					ROW_SOUND + " TEXT, " +
					ROW_MAX_SNOOZES + "  INT, " +
					ROW_SNOOZE_TIME + " INT, " +
					ROW_VIBRATE + " TEXT, " +
					ROW_LABEL + " TEXT" +
					");";
			DB.execSQL(newTableString);
			
		}
		public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
			// TODO Auto-generated method stub
			
		}
		
	}
}
