package com.twentyfourktapps.zombiealarmclock;



public class AlarmObject {

	boolean isEnabled, repeat, isVibrate;
	String label, sound;
	int alarmID, recID, maxSnoozes, snoozeTime;
	long timeMillis;
	
	public AlarmObject(int alarmId, int recId, String alarmLabel, boolean enabled, boolean isRepeat, long time, String soundString, boolean vibrateEnabled, int max_snoozes, int snooze_time) {
		alarmID = alarmId;
		recID = recId;
		isEnabled = enabled;
		repeat = isRepeat;
		timeMillis = time;
		label = alarmLabel;
		sound = soundString;
		isVibrate = vibrateEnabled;
		maxSnoozes = max_snoozes;
		snoozeTime = snooze_time;
	}
	
}
