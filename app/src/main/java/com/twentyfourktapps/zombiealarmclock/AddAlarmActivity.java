package com.twentyfourktapps.zombiealarmclock;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class AddAlarmActivity extends Activity {

	int alarmID = -1;
	boolean isNewAlarm = true;
	
	final String TAG = "AddAlarmActivity";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_alarm_dialog_layout);
		alarmID = getIntent().getExtras().getInt("com.twentyfourktapps.zombiealarmclock.AddAlarmActivity.alarmNum");
		isNewAlarm = getIntent().getExtras().getBoolean("com.twentyfourktapps.zombiealarmclock.AddAlarmActivity.isNew");
		if(!isNewAlarm) {
			populateViewsForUpdate();
			setTitle("Update Alarm");
		}
		setButtonListeners();
		setSeekbarListeners();
	}
	
	
	private void setButtonListeners() {
		Typeface font = Typeface.createFromAsset(getAssets(), "feasfbrg.ttf");
		Button saveButton = (Button)findViewById(R.id.add_alarm_button_done);
		saveButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				if(!isNewAlarm) {
					//remove current alarms before inserting new (Hard Update)
					DatabaseInterface DI = new DatabaseInterface(AddAlarmActivity.this);
					DI.deleteBundle(alarmID);
					stopService(new Intent(AddAlarmActivity.this, AlarmService.class));
					DI.closeDB();				
				}
				
				EditText labelET = (EditText)findViewById(R.id.add_alarm_edittext_label);
				String labelString = labelET.getText().toString();
				final Calendar now = Calendar.getInstance();
				Calendar newCal = Calendar.getInstance();
				final TimePicker timePicker = (TimePicker)findViewById(R.id.add_alarm_timepicker);
				newCal.set(Calendar.HOUR, timePicker.getCurrentHour());
				newCal.set(Calendar.MINUTE, timePicker.getCurrentMinute());

				
				SeekBar maxSnoozesSB = (SeekBar)findViewById(R.id.add_alarm_seekbar_maxsnoozes);
				SeekBar snoozeTimeSB = (SeekBar)findViewById(R.id.add_alarm_seekbar_snoozetime);
				Spinner soundSpinner = (Spinner)findViewById(R.id.add_alarm_spinner_sound);
				
				CheckBox monCheckbox = (CheckBox)findViewById(R.id.add_alarm_checkbox_monday);
				CheckBox tueCheckbox = (CheckBox)findViewById(R.id.add_alarm_checkbox_tuesday);
				CheckBox wedCheckbox = (CheckBox)findViewById(R.id.add_alarm_checkbox_wednesday);
				CheckBox thuCheckbox = (CheckBox)findViewById(R.id.add_alarm_checkbox_thursday);
				CheckBox friCheckbox = (CheckBox)findViewById(R.id.add_alarm_checkbox_friday);
				CheckBox satCheckbox = (CheckBox)findViewById(R.id.add_alarm_checkbox_saturday);
				CheckBox sunCheckbox = (CheckBox)findViewById(R.id.add_alarm_checkbox_sunday);
				CheckBox repeatCheckbox = (CheckBox)findViewById(R.id.add_alarm_checkbox_repeat);
				CheckBox vibrateCheckbox = (CheckBox)findViewById(R.id.add_alarm_checkbox_vibrate);
				
				boolean toRepeat = repeatCheckbox.isChecked();
				boolean toVibrate = vibrateCheckbox.isChecked();
				int maxSnoozes = maxSnoozesSB.getProgress();
				int snoozeTime = snoozeTimeSB.getProgress();
				String soundSelection = String.valueOf(soundSpinner.getSelectedItem());
				boolean isDefaultDay = true;

                boolean isTodayFirst;
				DatabaseInterface DI = new DatabaseInterface(getBaseContext());
				//Monday
				if(monCheckbox.isChecked()) {
					newCal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
					newCal.set(Calendar.HOUR_OF_DAY, timePicker.getCurrentHour());
					newCal.set(Calendar.MINUTE, timePicker.getCurrentMinute());
					newCal.set(Calendar.SECOND, 0);
					isTodayFirst = !newCal.before(now);
					if(!isTodayFirst) newCal.add(Calendar.WEEK_OF_YEAR, 1);
					//Log.e(TAG, "mon newCal :" + String.valueOf(newCal.get(Calendar.DAY_OF_WEEK)) + ", " + newCal.get(Calendar.HOUR_OF_DAY) + ":" + newCal.get(Calendar.MINUTE));
					DI.insertAlarm(alarmID, labelString, newCal.getTimeInMillis(), toRepeat, toVibrate, snoozeTime, maxSnoozes, soundSelection);
					newCal = Calendar.getInstance();
					isDefaultDay = false;
				}
				//Tuesday
				if(tueCheckbox.isChecked()) {
					newCal.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
					newCal.set(Calendar.HOUR_OF_DAY, timePicker.getCurrentHour());
					newCal.set(Calendar.MINUTE, timePicker.getCurrentMinute());
					newCal.set(Calendar.SECOND, 0);
					isTodayFirst = !newCal.before(now);
					if(!isTodayFirst) newCal.add(Calendar.WEEK_OF_YEAR, 1);
					//Log.e(TAG, "timepicker time = " + timePicker.getCurrentHour() + ":" + timePicker.getCurrentMinute());
					//Log.e(TAG, "tue newCal :" + String.valueOf(newCal.get(Calendar.DAY_OF_WEEK)) + ", " + newCal.get(Calendar.HOUR_OF_DAY) + ":" + newCal.get(Calendar.MINUTE));
					DI.insertAlarm(alarmID, labelString, newCal.getTimeInMillis(), toRepeat, toVibrate, snoozeTime, maxSnoozes, soundSelection);
					newCal = Calendar.getInstance();
					isDefaultDay = false;
				}
				//Wednesday
				if(wedCheckbox.isChecked()) {
					newCal.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
					newCal.set(Calendar.HOUR_OF_DAY, timePicker.getCurrentHour());
					newCal.set(Calendar.MINUTE, timePicker.getCurrentMinute());
					newCal.set(Calendar.SECOND, 0);
					isTodayFirst = !newCal.before(now);
					if(!isTodayFirst) newCal.add(Calendar.WEEK_OF_YEAR, 1);
					//Log.e(TAG, "wed newCal :" + String.valueOf(newCal.get(Calendar.DAY_OF_WEEK)) + ", " + newCal.get(Calendar.HOUR_OF_DAY) + ":" + newCal.get(Calendar.MINUTE));
					DI.insertAlarm(alarmID, labelString, newCal.getTimeInMillis(), toRepeat, toVibrate, snoozeTime, maxSnoozes, soundSelection);
					newCal = Calendar.getInstance();
					isDefaultDay = false;
				}
				//Thursday
				if(thuCheckbox.isChecked()) {
					newCal.set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY);
					newCal.set(Calendar.HOUR_OF_DAY, timePicker.getCurrentHour());
					newCal.set(Calendar.MINUTE, timePicker.getCurrentMinute());
					newCal.set(Calendar.SECOND, 0);
					isTodayFirst = !newCal.before(now);
					if(!isTodayFirst) newCal.add(Calendar.WEEK_OF_YEAR, 1);
					//Log.e(TAG, "thu newCal :" + String.valueOf(newCal.get(Calendar.DAY_OF_WEEK)) + ", " + newCal.get(Calendar.HOUR_OF_DAY) + ":" + newCal.get(Calendar.MINUTE));
					DI.insertAlarm(alarmID, labelString, newCal.getTimeInMillis(), toRepeat, toVibrate, snoozeTime, maxSnoozes, soundSelection);
					newCal = Calendar.getInstance();
					isDefaultDay = false;
				}
				//Friday
				if(friCheckbox.isChecked()) {
					newCal.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
					newCal.set(Calendar.HOUR_OF_DAY, timePicker.getCurrentHour());
					newCal.set(Calendar.MINUTE, timePicker.getCurrentMinute());
					newCal.set(Calendar.SECOND, 0);
					isTodayFirst = !newCal.before(now);
					if(!isTodayFirst) newCal.add(Calendar.WEEK_OF_YEAR, 1);
					//Log.e(TAG, "fri newCal :" + String.valueOf(newCal.get(Calendar.DAY_OF_WEEK)) + ", " + newCal.get(Calendar.HOUR_OF_DAY) + ":" + newCal.get(Calendar.MINUTE));
					DI.insertAlarm(alarmID, labelString, newCal.getTimeInMillis(), toRepeat, toVibrate, snoozeTime, maxSnoozes, soundSelection);
					newCal = Calendar.getInstance();
					isDefaultDay = false;
				}
				//Saturday
				if(satCheckbox.isChecked()) {
					newCal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
					newCal.set(Calendar.HOUR_OF_DAY, timePicker.getCurrentHour());
					newCal.set(Calendar.MINUTE, timePicker.getCurrentMinute());
					newCal.set(Calendar.SECOND, 0);
					isTodayFirst = !newCal.before(now);
					if(!isTodayFirst) newCal.add(Calendar.WEEK_OF_YEAR, 1);
					//Log.e(TAG, "sat newCal :" + String.valueOf(newCal.get(Calendar.DAY_OF_WEEK)) + ", " + newCal.get(Calendar.HOUR_OF_DAY) + ":" + newCal.get(Calendar.MINUTE));
					DI.insertAlarm(alarmID, labelString, newCal.getTimeInMillis(), toRepeat, toVibrate, snoozeTime, maxSnoozes, soundSelection);
					newCal = Calendar.getInstance();
					isDefaultDay = false;
				}
				//Sunday
				if(sunCheckbox.isChecked()) {
					newCal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
					newCal.set(Calendar.HOUR_OF_DAY, timePicker.getCurrentHour());
					newCal.set(Calendar.MINUTE, timePicker.getCurrentMinute());
					newCal.set(Calendar.SECOND, 0);
					isTodayFirst = !newCal.before(now);
					if(!isTodayFirst) newCal.add(Calendar.WEEK_OF_YEAR, 1);
					//Log.e(TAG, "sun newCal :" + String.valueOf(newCal.get(Calendar.DAY_OF_WEEK)) + ", " + newCal.get(Calendar.HOUR_OF_DAY) + ":" + newCal.get(Calendar.MINUTE));
					DI.insertAlarm(alarmID, labelString, newCal.getTimeInMillis(), toRepeat, toVibrate, snoozeTime, maxSnoozes, soundSelection);
					isDefaultDay = false;
				}
				
				if(isDefaultDay) {
					newCal = Calendar.getInstance();
					newCal.set(Calendar.HOUR_OF_DAY, timePicker.getCurrentHour());
					newCal.set(Calendar.MINUTE, timePicker.getCurrentMinute());
					newCal.set(Calendar.SECOND, 0);
					while(newCal.before(now)) {
						newCal.add(Calendar.DAY_OF_WEEK, 1);
						//newCal.add(Calendar.WEEK_OF_YEAR, 1);
					}
					DI.insertAlarm(alarmID, labelString, newCal.getTimeInMillis(), toRepeat, toVibrate, snoozeTime, maxSnoozes, soundSelection);
				}
				
				
				//DI.getNextAlarm(AddAlarmActivity.this);
				DI.logAllAlarms();
				DI.closeDB();
				Toast.makeText(getBaseContext(), "Alarm Saved", Toast.LENGTH_SHORT).show();
				onBackPressed();
		
			}
		});
		
		Button cancelButton = (Button)findViewById(R.id.add_alarm_button_cancel);
		cancelButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				onBackPressed();
				
			}
		});
	
		saveButton.setTypeface(font);
		cancelButton.setTypeface(font);
	}
	private void setSeekbarListeners() {
		final SeekBar maxSnoozesSB = (SeekBar)findViewById(R.id.add_alarm_seekbar_maxsnoozes);
		final SeekBar snoozeTimeSB = (SeekBar)findViewById(R.id.add_alarm_seekbar_snoozetime);
		final TextView maxSnoozesTV = (TextView)findViewById(R.id.add_alarm_textview_maxsnoozes);
		final TextView snoozeTimeTV = (TextView)findViewById(R.id.add_alarm_textview_snoozetime);
		
		
		//MaxSnoozes
		maxSnoozesSB.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				String valueString = Integer.toString(progress);				
				maxSnoozesTV.setText(valueString);
				
				if(progress < 1) snoozeTimeSB.setEnabled(false);
				else snoozeTimeSB.setEnabled(true);
				
			}
		});
		String maxSnoozesValueString = Integer.toString(maxSnoozesSB.getProgress());
		maxSnoozesTV.setText(maxSnoozesValueString);
		
		
		//SnoozeTime
		snoozeTimeSB.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {

				String valueString;
				if(progress < 1) {
					valueString = "1 min";
					snoozeTimeSB.setProgress(1);
				} else valueString = Integer.toString(progress) + " mins";
				snoozeTimeTV.setText(valueString);
				
			}
		});
		String snoozeTimeValueString = Integer.toString(snoozeTimeSB.getProgress()) + " mins";
		snoozeTimeTV.setText(snoozeTimeValueString);
		
	}
	private void populateViewsForUpdate() {
		DatabaseInterface DI = new DatabaseInterface(AddAlarmActivity.this);
		AlarmBundle alarm = DI.getAlarmBundleForId(alarmID);
		DI.closeDB();
		
		EditText labelET = (EditText)findViewById(R.id.add_alarm_edittext_label);
		labelET.setText(alarm.label);
		
		SeekBar maxSnoozesSB = (SeekBar)findViewById(R.id.add_alarm_seekbar_maxsnoozes);
		SeekBar snoozeTimeSB = (SeekBar)findViewById(R.id.add_alarm_seekbar_snoozetime);
		Spinner soundSpinner = (Spinner)findViewById(R.id.add_alarm_spinner_sound);
		
		maxSnoozesSB.setProgress(alarm.maxSnoozes);
		snoozeTimeSB.setProgress(alarm.snoozeTime);
		
		List<String> soundArrayList = Arrays.asList(getResources().getStringArray(R.array.sounds_array));
		int spinnerPos = soundArrayList.indexOf(alarm.soundString);
		soundSpinner.setSelection(spinnerPos);
		
		CheckBox monCheckbox = (CheckBox)findViewById(R.id.add_alarm_checkbox_monday);
		CheckBox tueCheckbox = (CheckBox)findViewById(R.id.add_alarm_checkbox_tuesday);
		CheckBox wedCheckbox = (CheckBox)findViewById(R.id.add_alarm_checkbox_wednesday);
		CheckBox thuCheckbox = (CheckBox)findViewById(R.id.add_alarm_checkbox_thursday);
		CheckBox friCheckbox = (CheckBox)findViewById(R.id.add_alarm_checkbox_friday);
		CheckBox satCheckbox = (CheckBox)findViewById(R.id.add_alarm_checkbox_saturday);
		CheckBox sunCheckbox = (CheckBox)findViewById(R.id.add_alarm_checkbox_sunday);
		CheckBox repeatCheckbox = (CheckBox)findViewById(R.id.add_alarm_checkbox_repeat);
		CheckBox vibrateCheckbox = (CheckBox)findViewById(R.id.add_alarm_checkbox_vibrate);
		
		monCheckbox.setChecked(alarm.isMon);
		tueCheckbox.setChecked(alarm.isTue);
		wedCheckbox.setChecked(alarm.isWed);
		thuCheckbox.setChecked(alarm.isThu);
		friCheckbox.setChecked(alarm.isFri);
		satCheckbox.setChecked(alarm.isSat);
		sunCheckbox.setChecked(alarm.isSun);
		repeatCheckbox.setChecked(alarm.repeat);
		vibrateCheckbox.setChecked(alarm.vibrate);
		
		TimePicker timePicker = (TimePicker)findViewById(R.id.add_alarm_timepicker);
		long alarmTime = alarm.alarms.get(0).timeMillis;
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(alarmTime);
		timePicker.setCurrentHour(c.get(Calendar.HOUR_OF_DAY));
		timePicker.setCurrentMinute(c.get(Calendar.MINUTE));
	}

}
