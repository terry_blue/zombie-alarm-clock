package com.twentyfourktapps.zombiealarmclock;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.twentyfourktapps.zombiealarmclock.R;

public class BundleArrayAdaptor extends ArrayAdapter<AlarmBundle>{

	AlarmBundle[] data;
	int layoutID;
	Context context;
	final String TAG = "BundleArrayAdaptor";
	MainActivity activity;
	
	public BundleArrayAdaptor(Context pContext, int layoutResourceId, AlarmBundle[] alarmArray, MainActivity pActivity) {
		super(pContext, layoutResourceId, alarmArray);
		data = alarmArray;
		layoutID = layoutResourceId;
		context = pContext;
		activity = pActivity;
	}
	
	@Override
	public View getView(final int pos, View convertView, ViewGroup parent) {
		View row = convertView;
		BundleHolder holder;
		
		if(row == null) {
			 LayoutInflater inflater = ((Activity)context).getLayoutInflater();
	         row = inflater.inflate(layoutID, parent, false);
	         
	         holder = new BundleHolder();
	         holder.flagMon = (TextView)row.findViewById(R.id.alarm_bundle_textview_flag_mon);
	         holder.flagTue = (TextView)row.findViewById(R.id.alarm_bundle_textview_flag_tue);
	         holder.flagWed = (TextView)row.findViewById(R.id.alarm_bundle_textview_flag_wed);
	         holder.flagThu = (TextView)row.findViewById(R.id.alarm_bundle_textview_flag_thu);
	         holder.flagFri = (TextView)row.findViewById(R.id.alarm_bundle_textview_flag_fri);
	         holder.flagSat = (TextView)row.findViewById(R.id.alarm_bundle_textview_flag_sat);
	         holder.flagSun = (TextView)row.findViewById(R.id.alarm_bundle_textview_flag_sun);
	         holder.label = (TextView)row.findViewById(R.id.alarm_bundle_textview_label);
	         holder.time = (TextView)row.findViewById(R.id.alarm_bundle_textview_time);
	         holder.imageFlag = (ImageView)row.findViewById(R.id.alarm_bundle_layout_imageview_indicator);
	         holder.imageFlagContainer = (LinearLayout)row.findViewById(R.id.alarm_bundle_layout_linearlayout_imageindicator);
	         holder.leftContainer = (LinearLayout)row.findViewById(R.id.alarm_bundle_linearlayout_leftside);
	         
	         row.setTag(holder);
		} else {
			holder = (BundleHolder)row.getTag();
		}
		
		AlarmBundle bundle = data[pos];
		
		holder.label.setText(bundle.label);
		
		SimpleDateFormat df = new SimpleDateFormat("h:mm a");
		String timeString = df.format(new Date(bundle.alarms.get(0).timeMillis));
		holder.time.setText(timeString);
		
		if(bundle.isEnabled) holder.imageFlag.setVisibility(View.VISIBLE);
		else holder.imageFlag.setVisibility(View.INVISIBLE);
		
		if(bundle.isMon) holder.flagMon.setTextAppearance(getContext(), R.style.bundle_day_tag_style_active);
		else holder.flagMon.setTextAppearance(getContext(), R.style.bundle_day_tag_style);
		if(bundle.isTue) holder.flagTue.setTextAppearance(getContext(), R.style.bundle_day_tag_style_active);
		else holder.flagTue.setTextAppearance(getContext(), R.style.bundle_day_tag_style);
		if(bundle.isWed) holder.flagWed.setTextAppearance(getContext(), R.style.bundle_day_tag_style_active);
		else holder.flagWed.setTextAppearance(getContext(), R.style.bundle_day_tag_style);
		if(bundle.isThu) holder.flagThu.setTextAppearance(getContext(), R.style.bundle_day_tag_style_active);
		else holder.flagThu.setTextAppearance(getContext(), R.style.bundle_day_tag_style);
		if(bundle.isFri) holder.flagFri.setTextAppearance(getContext(), R.style.bundle_day_tag_style_active);
		else holder.flagFri.setTextAppearance(getContext(), R.style.bundle_day_tag_style);
		if(bundle.isSat) holder.flagSat.setTextAppearance(getContext(), R.style.bundle_day_tag_style_active);
		else holder.flagSat.setTextAppearance(getContext(), R.style.bundle_day_tag_style);
		if(bundle.isSun) holder.flagSun.setTextAppearance(getContext(), R.style.bundle_day_tag_style_active);
		else holder.flagSun.setTextAppearance(getContext(), R.style.bundle_day_tag_style);
		
		
		holder.imageFlagContainer.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				DatabaseInterface DI = new DatabaseInterface(context);
				DI.setBundleEnabled(data[pos].alarmID, !data[pos].isEnabled);
				ArrayList<AlarmBundle> bundles = DI.getAlarmBundles();
				data = bundles.toArray(new AlarmBundle[bundles.size()]);
				DI.closeDB();
				activity.setNextAlarmView();
				notifyDataSetChanged();
			}
		});
		
		
		return row;
	}

	
	static class BundleHolder {
		TextView flagMon, flagTue, flagWed, flagThu, flagFri, flagSat, flagSun, label, time;
		ImageView imageFlag;
		LinearLayout imageFlagContainer, leftContainer;
	}
	
	 public void enabledViewClickEvent(View v) {
		   Log.e(TAG, "enabledViewClickEvent() fired");
	   }
}
