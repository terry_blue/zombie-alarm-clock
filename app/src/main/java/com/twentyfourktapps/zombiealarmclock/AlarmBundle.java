package com.twentyfourktapps.zombiealarmclock;

import java.util.ArrayList;
import java.util.Calendar;

public class AlarmBundle {

	ArrayList<AlarmObject> alarms;
	boolean isMon, isTue, isWed, isThu, isFri, isSat, isSun, vibrate, repeat, isEnabled;
	long alarmTime, lastAlarmTime;
	String soundString, label;
	int maxSnoozes, snoozeTime, alarmID;
	
	public AlarmBundle(ArrayList<AlarmObject> alarmObjects) {
		alarms = alarmObjects;
		setDayFlags();
		alarmTime = alarms.get(0).timeMillis;
		lastAlarmTime = alarms.get(alarms.size() - 1).timeMillis;
		soundString = alarms.get(0).sound;
		label = alarms.get(0).label;
		vibrate = alarms.get(0).isVibrate;
		repeat = alarms.get(0).repeat;
		isEnabled = alarms.get(0).isEnabled;
		maxSnoozes = alarms.get(0).maxSnoozes;
		snoozeTime = alarms.get(0).snoozeTime;
		alarmID = alarms.get(0).alarmID;
		
	}
	
	private void setDayFlags() {
		isMon = false;
		isTue = false;
		isWed = false;
		isThu = false;
		isFri = false;
		isSat = false;
		isSun = false;
		
		Calendar cal = Calendar.getInstance();
		
		for(int i = 0; i < alarms.size(); i++) {
			cal.setTimeInMillis(alarms.get(i).timeMillis);
			if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) isSun = true;
			else if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) isMon = true;
			else if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY) isTue = true;
			else if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY) isWed = true;
			else if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY) isThu = true;
			else if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) isFri = true;
			else if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) isSat = true;
		}
	}
}
