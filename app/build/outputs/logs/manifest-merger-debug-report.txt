-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:1:1
	package
		ADDED from AndroidManifest.xml:2:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionName
		ADDED from AndroidManifest.xml:4:5
	android:versionCode
		ADDED from AndroidManifest.xml:3:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	xmlns:android
		ADDED from AndroidManifest.xml:1:11
uses-sdk
ADDED from AndroidManifest.xml:6:5
	android:targetSdkVersion
		ADDED from AndroidManifest.xml:8:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:minSdkVersion
		ADDED from AndroidManifest.xml:7:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
uses-permission#android.permission.WAKE_LOCK
ADDED from AndroidManifest.xml:10:2
	android:name
		ADDED from AndroidManifest.xml:10:19
uses-permission#android.permission.VIBRATE
ADDED from AndroidManifest.xml:11:2
	android:name
		ADDED from AndroidManifest.xml:11:19
application
ADDED from AndroidManifest.xml:13:5
	android:label
		ADDED from AndroidManifest.xml:15:9
	android:icon
		ADDED from AndroidManifest.xml:14:9
	android:theme
		ADDED from AndroidManifest.xml:16:9
activity#com.twentyfourktapps.zombiealarmclock.MainActivity
ADDED from AndroidManifest.xml:17:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:19:13
	android:theme
		ADDED from AndroidManifest.xml:20:13
	android:name
		ADDED from AndroidManifest.xml:18:13
intent-filter#android.intent.action.MAIN+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:21:13
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:22:17
	android:name
		ADDED from AndroidManifest.xml:22:25
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:24:17
	android:name
		ADDED from AndroidManifest.xml:24:27
receiver#com.twentyfourktapps.zombiealarmclock.AppletWidgetProvider
ADDED from AndroidManifest.xml:28:9
	android:label
		ADDED from AndroidManifest.xml:28:56
	android:name
		ADDED from AndroidManifest.xml:28:19
intent-filter#android.appwidget.action.APPWIDGET_UPDATE+com.tfkt.widget.clock_WIDGET_UPDATE
ADDED from AndroidManifest.xml:29:13
action#android.appwidget.action.APPWIDGET_UPDATE
ADDED from AndroidManifest.xml:30:17
	android:name
		ADDED from AndroidManifest.xml:30:25
action#com.tfkt.widget.clock_WIDGET_UPDATE
ADDED from AndroidManifest.xml:31:17
	android:name
		ADDED from AndroidManifest.xml:31:25
meta-data#android.appwidget.provider
ADDED from AndroidManifest.xml:33:13
	android:resource
		ADDED from AndroidManifest.xml:33:66
	android:name
		ADDED from AndroidManifest.xml:33:24
activity#com.twentyfourktapps.zombiealarmclock.AlarmActivity
ADDED from AndroidManifest.xml:37:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:37:49
	android:theme
		ADDED from AndroidManifest.xml:37:86
	android:name
		ADDED from AndroidManifest.xml:37:19
activity#com.twentyfourktapps.zombiealarmclock.AddAlarmActivity
ADDED from AndroidManifest.xml:38:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:39:13
	android:label
		ADDED from AndroidManifest.xml:40:13
	android:windowSoftInputMode
		ADDED from AndroidManifest.xml:41:13
	android:name
		ADDED from AndroidManifest.xml:38:19
service#com.twentyfourktapps.zombiealarmclock.AlarmService
ADDED from AndroidManifest.xml:42:9
	android:name
		ADDED from AndroidManifest.xml:42:18
